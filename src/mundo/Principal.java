package mundo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import Estructuras.Lista;


public class Principal {

	private Lista<Aerolinea> aerolineas;
	private Lista<Vuelo> vuelos;
	private Lista<Ciudad> ciudades;

	public Principal() {
		super();
		// TODO Auto-generated constructor stub
		try {
			aerolineas = new Lista<>();
			vuelos = new Lista<>();
			ciudades = new Lista<>();
			cargarInfo();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Hay "+aerolineas.darTamanio()+" aerolineas");
		System.out.println("Hay "+vuelos.darTamanio()+" vuelos");
		System.out.println("Hay "+ciudades.darTamanio()+" ciudades");

		confirmarLectura();
	}
	public Lista<Aerolinea> getAerolineas() {
		return aerolineas;
	}
	public Lista<Vuelo> getVuelos() {
		return vuelos;
	}

	public void confirmarLectura()
	{
		Iterator<Ciudad> it = ciudades.iterator();
		while(it.hasNext())
		{
			Ciudad actual = it.next();

			System.out.println("------");
			System.out.println(actual.toString());
			System.out.println("------");

		}

		System.out.println("______________________________________________");
		Iterator<Aerolinea> it3 = aerolineas.iterator();
		while(it3.hasNext())
		{
			Aerolinea actual = it3.next();

			System.out.println("------");
			System.out.println(actual.toString());
			System.out.println("------");

		}

		System.out.println("______________________________________________");
		Iterator<Vuelo> it2 = vuelos.iterator();
		while(it2.hasNext())
		{
			Vuelo actual = it2.next();

			System.out.println("------");
			System.out.println(actual.toString());
			System.out.println("------");


		}
	}

	public void cargarInfo() throws Exception
	{
		BufferedReader bReader = new BufferedReader(new FileReader(new File("./data/minAerolinea.txt")));


		String linea = bReader.readLine();


		while (linea!= null)
		{

			System.out.println(linea);
			String[] datos = linea.split(";");
			String nombre = datos[0];
			double costo = Double.parseDouble(datos[1]);
			int numMax = Integer.parseInt(datos[2]);
			Aerolinea nueva = new Aerolinea(nombre, costo, numMax);
			aerolineas.encolar(nueva);

			linea = bReader.readLine();
		}

		bReader.close();

		bReader = new BufferedReader(new FileReader(new File("./data/vuelos.txt")));

		linea = bReader.readLine();


		while (linea!= null)
		{

			System.out.println(linea);
			String[] datos = linea.split(";");
			String nombreAe = datos[0];
			Aerolinea aer = buscarAerolinea(nombreAe);
			String numVuelo = datos[1];



			String nomCiudadOr = datos[2];
			Ciudad ciudadOrigen = buscarCiudad(nomCiudadOr);
			if(ciudadOrigen== null)
			{
				ciudadOrigen = new Ciudad(nomCiudadOr);
				ciudades.encolar(ciudadOrigen);
			}

			String nomCiudadDest = datos[3];
			Ciudad ciudadDestino = buscarCiudad(nomCiudadDest);
			if(ciudadDestino== null)
			{
				ciudadDestino = new Ciudad(nomCiudadDest);
				ciudades.encolar(ciudadDestino);
			}


			DateFormat FInicio = new SimpleDateFormat("HH:mm");
			Date hInicio = FInicio.parse(datos[4]);
			DateFormat FFin = new SimpleDateFormat("HH:mm");
			Date hFin = FFin.parse(datos[5]);
			String tipoEquipo = datos[6];
			int numSillas = Integer.parseInt(datos[7]);
			String tipoVuelo = datos[15];

			for(int i = 8; i<=14;i++)
			{
				String fecha = datos[i];
				if(fecha.equals("X"))
				{
					Vuelo vuelo = new Vuelo(aer, numVuelo, ciudadOrigen, ciudadDestino, i-7, tipoVuelo, tipoEquipo, numSillas,hInicio,hFin);
					vuelos.encolar(vuelo);
				}
			}

			linea = bReader.readLine();
		}
		bReader.close();

	}

	public Ciudad buscarCiudad(String nombre)
	{
		Iterator<Ciudad> it = ciudades.iterator();
		while(it.hasNext())
		{
			Ciudad actual = it.next();
			if(actual.getNombre().equals(nombre))
			{
				return actual;
			}
		}

		return null;
	}

	public Aerolinea buscarAerolinea(String nombre)
	{
		Iterator<Aerolinea> it = aerolineas.iterator();
		while(it.hasNext())
		{
			Aerolinea actual = it.next();
			if(actual.getNombre().equals(nombre))
			{
				return actual;
			}
		}

		return null;
	}
	public static void main(String[] args) throws Exception {
		Principal p = new Principal();

	}
}
