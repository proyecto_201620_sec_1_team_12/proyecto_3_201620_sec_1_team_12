package mundo;

import Estructuras.Lista;

public class Ciudad {

	private String nombre;
	private Lista<Vuelo> vuelosSalida;
	
	
	public Ciudad(String nombre, Lista<Vuelo> vuelosSalida) {
		super();
		this.nombre = nombre;
		this.vuelosSalida = vuelosSalida;
	}
	
	public Ciudad(String nombre2) {
		// TODO Auto-generated constructor stub
		this.nombre = nombre2;
	}

	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public Lista<Vuelo> getVuelosSalida() {
		return vuelosSalida;
	}
	
	public void setVuelosSalida(Lista<Vuelo> vuelosSalida) {
		this.vuelosSalida = vuelosSalida;
	}

	@Override
	public String toString() {
		return "Ciudad [nombre=" + nombre + "]";
	}
	
	
}
