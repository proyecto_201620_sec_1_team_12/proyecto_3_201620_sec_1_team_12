package mundo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.print.DocFlavor.STRING;

public class Vuelo {

	public static int LUNES = 1;
	public static int MARTES = 2;
	public static int MIERCOLES = 3;
	public static int JUEVES = 4;
	public static int VIERNES = 5;
	public static int SABADO =6;
	public static int DOMINGO = 7;
	
	public static String INTERNACIONAL = "INT";
	public static String NACIONAL = "Nacional";
	
	
	private Aerolinea aerolinea;
	private String numVuelo;
	private Ciudad ciudadOrigen;
	private Ciudad ciudadDestino;
	private int diaSemana;
	private String tipoVuelo;
	private String tipoEquipo;
	private Date horaSalida;
	private Date horaLlegada;
	private int numSillas;
	
	
	public Vuelo(Aerolinea aerolinea, String numVuelo, Ciudad ciudadOrigen, Ciudad ciudadDestino, int diaSemana,
			String tipoVuelo, String tipoEquipo, int numSillas, Date horaSalida, Date horaLlegada) {
		super();
		this.aerolinea = aerolinea;
		this.numVuelo = numVuelo;
		this.ciudadOrigen = ciudadOrigen;
		this.ciudadDestino = ciudadDestino;
		this.diaSemana = diaSemana;
		this.tipoVuelo = tipoVuelo;
		this.tipoEquipo = tipoEquipo;
		this.numSillas = numSillas;
		this.horaSalida = horaSalida;
		this.horaLlegada = horaLlegada;
	}
	
	public String toString()
	{
    	DateFormat outFormat = new SimpleDateFormat( "HH:mm");

		String horaS = outFormat.format(horaSalida);
		String horaL = outFormat.format(horaLlegada);
		
		return " Aerolinea: "+ aerolinea.getNombre() +"\n Num Vuelo: "+ numVuelo+ "\n Origen: "+ciudadOrigen.getNombre()+ "\n Destino: "+ciudadDestino.getNombre()+
				"\n Dia semana: "+diaSemana+ "\n Hora salida: "+horaS +"\n Hora Llegada: "+horaL+"\n Tipo Vuelo: "+tipoVuelo +"\n Tipo Equipo: "+tipoEquipo ;
	}
	
	
}
