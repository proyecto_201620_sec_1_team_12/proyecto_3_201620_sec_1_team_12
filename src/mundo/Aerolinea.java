package mundo;

import Estructuras.Lista;

public class Aerolinea {

	private String nombre;
	private Lista<Vuelo> vuelos;
	private double valorMinuto;
	private int numSillasMax;
	
	public Aerolinea(String nombre, double valorMinuto, int numSillasMax) {
		super();
		this.nombre = nombre;
		
		this.valorMinuto = valorMinuto;
		this.numSillasMax = numSillasMax;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Lista<Vuelo> getVuelos() {
		return vuelos;
	}

	public void setVuelos(Lista<Vuelo> vuelos) {
		this.vuelos = vuelos;
	}

	public double getValorMinuto() {
		return valorMinuto;
	}

	public void setValorMinuto(double valorMinuto) {
		this.valorMinuto = valorMinuto;
	}

	public int getNumSillasMax() {
		return numSillasMax;
	}

	public void setNumSillasMax(int numSillasMax) {
		this.numSillasMax = numSillasMax;
	}

	@Override
	public String toString() {
		return "Aerolinea [nombre=" + nombre + ", valorMinuto=" + valorMinuto + ", numSillasMax=" + numSillasMax + "]";
	}

	
	
	
	
}
