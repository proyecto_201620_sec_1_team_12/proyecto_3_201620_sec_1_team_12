package Estructuras;


/**
 * Basado en Algorithm 4th edition
 * @author Alejandro Imbet
 *
 * @param <T>
 */
public class HeapLista<T extends Comparable<T>> implements IHeap<T>{

	/**
	 * Atributo que modela la estructura lineal contenedora de datos
	 */
	private Lista<T> values;

	/**
	 * Constructor del Heap
	 */
	public HeapLista() {
		values = new Lista<T>();
		values.encolar(null);;
	}

	@Override
	public void add(T elemento) {
		// TODO Auto-generated method stub
		values.encolar(elemento);
		siftUp();
	}

	@Override
	public T peek() {
		return values.get(1);
	}

	@Override
	public T poll() {
		T val = values.get(1);
		exch(1, size());
		values.remove(size());
		siftDown();

		return val;
	}

	@Override
	public int size() {
		return values.size()-1;
	}

	@Override
	public boolean isEmpty() {
		return values.isEmpty();
	}

	@Override
	public void siftUp() {

		int index = values.size()-1;

		while(index>>1>=1){
			T actual = values.get(index);
			T siguiente = values.get(index>>1);
			
			if(actual.compareTo(siguiente)==1)
			{
				exch(index,index>>1);
				index >>=1;
				continue;
			}
			break;
		}
	}

	@Override
	public void siftDown() {

		int index = 1;

		while(index<<1<=size()){
			if((index<<1)+1 >size()){
				if(values.get(index<<1).compareTo(values.get(index))==1)
				{
					exch(index,index<<1);
					index <<= 1;
					continue;
				}
			}
			else
			{
				int j = index<<1;
				if(values.get(j).compareTo(values.get(j+1))==-1)
				{
					j++;
				}
				if(values.get(j).compareTo(values.get(index))==-1)
				{
					break;
				}
				exch(j,index);
				index = j;
				continue;
			}
			break;
		}

	}

	/**
	 * Método que retorna los valores del Heap manteniendo el orden
	 * @return Valores del Heap en un arreglo
	 */
	public T[] values(){

		return null;
	}

	private void exch(int index, int index2) {

		T temp = values.get(index);
		values.set(index, values.get(index2));
		values.set(index2, temp);
	}

}
