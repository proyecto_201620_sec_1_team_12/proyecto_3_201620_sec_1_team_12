package Estructuras;


public class NodoRedBlack {
	
	private SeparateChainingHashST<Integer, Integer> separateChaining;
	
	private LinearProbingHashST<Integer, Integer> linearProbing;

	public NodoRedBlack(){
		separateChaining = null;
		linearProbing = null;
	}

	public SeparateChainingHashST<Integer, Integer> getSeparateChaining() {
		return separateChaining;
	}

	public LinearProbingHashST<Integer, Integer> getLinearProbing() {
		return linearProbing;
	}

	public void setSeparateChaining(SeparateChainingHashST<Integer, Integer> separateChaining) {
		this.separateChaining = separateChaining;
	}

	public void setLinearProbing(LinearProbingHashST<Integer, Integer> linearProbing) {
		this.linearProbing = linearProbing;
	}
	
	
}

